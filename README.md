# Rubiks Cube Simulation

![Rubiks Cube Animation](/images/animation.gif)

## Requirements
- Rubics Cube Simulation without Engine
- using OpenGL and C++
- using glew
- using glfw
- using glm
- using stb

## Goals
- lern the basics of OpenGl and the use of linear algebra.

## Getting started
Doubleclick on the `RubiksCube.exe`in the `Release` folder

## Control
- `q` turn the left X-disc upwards
- `w` turn the middle X-disc upwards
- `e` turn the right X-disc upwards
- `y` turn the left X-disc downwards
- `x` turn the middle X-disc downwards
- `c` turn the right X-disc downwards
- `shift` + `q` turn the upper Y-disc to the left
- `shift` + `a`	turn the middle Y-disc to the left
- `shift` + `y`	turn the lower Y-disc to the left
- `shift` + `e`	turn the upper Y-disc to the right
- `shift` + `d`	turn the middle Y-disc to the right
- `shift` + `c`	turn the lower Y-disc to the right
- `ctrl` + `q` turn the upper Z-disc to the left
- `ctrl` + `a`	turn the middle Z-disc to the left
- `ctrl` + `y`	turn the lower Z-disc to the left
- `ctrl` + `e`	turn the upper Z-disc to the right
- `ctrl` + `d`	turn the middle Z-disc to the right
- `ctrl` + `c`	turn the lower Z-disc to the right
- `arrow up` turn the camera upwards
- `arrow down` turn the camera downwards
- `arrow left` turn the camera left
- `arrow right` turn the camera right
- `space` reset camera position
- hold the `right mouse button` to turn the camera in the direction of the mouse position

## Future Work
- mouse control for disc movment