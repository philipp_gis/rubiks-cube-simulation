#version 330

uniform sampler2D sampler;

in vec3 sColor;
in vec2 sTexture;

out vec4 resultat;

void main()
{
    vec4 texture = texture(sampler, sTexture);
	vec4 color = vec4(sColor, 1.0f);

	resultat = texture * color;
}

