#pragma once

#include "KeyboardObserver.h"
#include <map>
#include <glm/mat4x4.hpp>

class InputSystem
{
public:
	InputSystem() { m_window = nullptr; }
	
	// Setzt Fenster das betrachtet wird
	void SetWindow(GLFWwindow* window) { m_window = window; }
	void Update();
	
	// Tasten die betrachtet werden sollen
	void ObserveKey(int key);

	// Taste ist unten
	bool IsKeyDown(int key) { return m_keyMapper[key].m_isDown; }
	
	// Taste wird unten gehalten
	bool WasKeyPressed(int key) { return m_keyMapper[key].m_wasPressed; }

	// Linke Maustasten wurden gedrückt
	bool IsLeftMouseButtonDown();
	bool IsRightMouseButtonDown();

	// Strahl der in Weltkoordinaten geschossen wird
	void GetPickingRay(const glm::mat4& transformationMatrix, glm::vec3& startingPoint, glm::vec3& direction);
	
	// Position der Maus relativ zum Seitenverhältnis des Fensters
	void GetMousePosition(double& xPosition, double& yPositon);

private:
	std::map<int, KeyboardObserver> m_keyMapper;
	GLFWwindow* m_window;
};