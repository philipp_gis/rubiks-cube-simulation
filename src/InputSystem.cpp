#include "InputSystem.h"
#include <GLFW\glfw3.h>

void InputSystem::Update()
{
	for (auto i = m_keyMapper.begin(); i != m_keyMapper.end(); ++i)
		i->second.Update();
}

void InputSystem::ObserveKey(int key)
{
	// Welche Taste wird überwacht
	m_keyMapper[key] = KeyboardObserver(m_window, key);
}

bool InputSystem::IsLeftMouseButtonDown()
{
	// Ist linke Maustaste gedrückt?
	return (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
}

bool InputSystem::IsRightMouseButtonDown()
{
	// Ist linke Maustaste gedrückt?
	return (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS);
}

void InputSystem::GetPickingRay(const glm::mat4& transformationMatrix, glm::vec3& startingPoint, glm::vec3& direction)
{
	// Mausposition
	double xPosition;
	double yPosition;
	
	GetMousePosition(xPosition, yPosition);

	// Vordere und hintere Ebene des Frustums
	glm::vec4 nearPoint = glm::vec4((float)xPosition, (float)yPosition, 0.01f, 1.0f);
	glm::vec4 farPoint = nearPoint;
	farPoint.z = 0.99f;

	// Setzt nearPoint und farPoint in Weltkoordinaten
	glm::mat4 inverse = glm::inverse(transformationMatrix);
	nearPoint = inverse * nearPoint;
	farPoint = inverse * farPoint;

	// dehomolisuerung
	nearPoint /= nearPoint.w;
	farPoint /= farPoint.w;

	// Startpunkt und Richtung
	startingPoint = nearPoint;
	direction = farPoint - nearPoint;

	direction = glm::normalize(direction);
}

void InputSystem::GetMousePosition(double& xPosition, double& yPosition)
{
	// Mausposition
	glfwGetCursorPos(m_window, &xPosition, &yPosition);

	// Seitenverhältnis des Fensters
	int screenWidth;
	int screenHeight;
	glfwGetFramebufferSize(m_window, &screenWidth, &screenHeight);

	xPosition = (xPosition / screenWidth) * 2.0 - 1.0;
	yPosition = 1.0 - (yPosition / screenHeight) * 2.0;
}