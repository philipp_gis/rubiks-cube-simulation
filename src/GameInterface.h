#pragma once
struct GLFWwindow;
	
// Interface um z�gig verschiedene Testzust�nde zu erzeugen
class GameInterface
{
public:
	// F�hrt das Programm in ein Anfagnszustand aus
	virtual void Initialize() {};
	virtual void Initialize(GLFWwindow* window) { Initialize(); };

	// F�hrt eine periodische aktuellisierung der Logik des Programmes aus
	virtual void Update(double deltaTime) {};

	// F�hrt eine periodische aktuellisierung der Grafik des Programmes aus
	virtual void Render(float aspectRatio) {};

	// Beendet das Programm in einem sauberen Endzustand
	virtual void ClearResources() {};
};

