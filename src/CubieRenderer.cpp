#include "CubieRenderer.h"
#include "ShaderUtil.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>


#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

void CubieRenderer::Initialize()
{
	// 6 Seiten, 6 Vertices, 3 Werte, 
	float floatArray[6 * 6 * 3];

	// Positionen, Farben, Texturkoordinaten
	std::vector<glm::vec3> positionField;
	std::vector<glm::vec3> colorField;
	std::vector<glm::vec3> textureField;

	// Erzeugt f�r jede Seite eines Cubie die Position, Farbe, Texturkoordinaten
	for (int sideType = 0; sideType < 3; ++sideType)
	{
		for (int direction = -1; direction < 2; direction += 2)
		{
			AddSidePosition(sideType, direction, positionField);
			AddSideColor(sideType, direction, colorField);
			AddSideTexture(sideType, direction, textureField);
		}
	}

	// ShaderProgramm erzeugen
	m_shaderProgram = ShaderUtil::CreateShaderProgram("VColor.glsl", "FColor.glsl");
	m_transformLocation = glGetUniformLocation(m_shaderProgram, "transformation");

	// �bergebe 1 ArrayBuffer und 3 VertexBufferObjects
	glGenVertexArrays(1, &m_arrayBufferObject);
	glGenBuffers(3, m_vertexBufferObject);
	glBindVertexArray(m_arrayBufferObject);

	// Binde ersten VertexBuffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[0]);
	TranscribeToFloatArray(positionField, floatArray);
	glBufferData(GL_ARRAY_BUFFER, sizeof floatArray, floatArray, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// Binde zweiten VertexBuffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[1]);
	TranscribeToFloatArray(colorField, floatArray);
	glBufferData(GL_ARRAY_BUFFER, sizeof floatArray, floatArray, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);

	// Binde dritten VertexBuffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject[2]);
	TranscribeToFloatArray(textureField, floatArray);
	glBufferData(GL_ARRAY_BUFFER, sizeof floatArray, floatArray, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(2);

	// Entbinde die 4 genutzten Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Textur laden
	int w;
	int h;
	int comp;
	unsigned char* image = stbi_load("texture.jpg", &w, &h, &comp, STBI_rgb_alpha);

	// Binde Textur
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	// Erzeuge Textur
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	// Entbinde Textur 
	glBindTexture(GL_TEXTURE_2D, 0);

	// Gebe Textur frei
	stbi_image_free(image);
}

void CubieRenderer::Render(const glm::mat4& transformationMatrix)
{
	// Programm binden
	glUseProgram(m_shaderProgram);
	
	// Buffer werden eingeladen
	glBindVertexArray(m_arrayBufferObject);

	// Transformationsmatrix einladen
	glUniformMatrix4fv(m_transformLocation, 1, GL_FALSE, value_ptr(transformationMatrix));
	
	// Textur wird gestartet
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	// Zeichne 36 Vertices
	glDrawArrays(GL_TRIANGLES, 0, 6 * 6);

	// Bindungen l�sen
	glBindVertexArray(0);
	glUseProgram(0);
}

void CubieRenderer::ClearResources()
{
	// Buffer, Programm, Textur wird gel�scht
	glDeleteBuffers(3, m_vertexBufferObject);
	glDeleteVertexArrays(1, &m_arrayBufferObject);
	glDeleteProgram(m_shaderProgram);
	glDeleteTextures(1, &m_texture);
}

void CubieRenderer::AddSidePosition(int sideType, int direction, std::vector<glm::vec3>& positionArray)
{
	glm::vec3 cornerPoints[2][2];

	// Die nicht �bergebenen Achsen
	int localXDim = (sideType + 1) % 3;
	int localYDim = (sideType + 2) % 3;

	// Eckpunkte werden erzeugt
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			glm::vec3 localPoint(direction * m_offset);
			localPoint[localXDim] = (2 * i - 1) * m_offset;
			localPoint[localYDim] = (2 * j - 1) * m_offset;
			cornerPoints[i][j] = localPoint;
		}
	}

	// Beide Dreiecke werden �bergeben
	positionArray.push_back(cornerPoints[0][0]);
	positionArray.push_back(cornerPoints[1][0]);
	positionArray.push_back(cornerPoints[0][1]);
	positionArray.push_back(cornerPoints[1][0]);
	positionArray.push_back(cornerPoints[0][1]);
	positionArray.push_back(cornerPoints[1][1]);
}

void CubieRenderer::AddSideColor(int sideType, int direction, std::vector<glm::vec3>& colorArray)
{
	glm::vec3 color = glm::vec3(0.0f);

	// Farben f�r jeweilige Seite des W�rfels
	if (direction > 0)
	{
		if (sideType == 0) { color = glm::vec3(0.0f, 0.0f, 1.0f); }
		if (sideType == 1) { color = glm::vec3(1.0f, 0.0f, 0.0f); }
		if (sideType == 2) { color = glm::vec3(1.0f, 1.0f, 0.0f); }
	}

	if (direction < 0)
	{
		if (sideType == 0) { color = glm::vec3(0.0f, 1.0f, 0.0f); }
		if (sideType == 1) { color = glm::vec3(1.0f, 0.5f, 0.0f); }
		if (sideType == 2) { color = glm::vec3(1.0f, 1.0f, 1.0f); }
	}

	// Farben werden �begeben
	for (int i = 0; i < 6; ++i) { colorArray.push_back(color); }
}

void CubieRenderer::AddSideTexture(int sideType, int direction, std::vector<glm::vec3>& textureArray)
{
	glm::vec3 cornerPoints[2][2];

	//  Erzeuge Texturkoordinaten
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
				glm::vec3 localPoint(direction * m_offset);
				localPoint[0] = (2 * i - 1) * m_offset;
				localPoint[1] = (2 * j - 1) * m_offset;
				cornerPoints[i][j] = localPoint;
		}
	}

	// �bergabe der Texturkoordinaten
	textureArray.push_back(cornerPoints[0][0]);
	textureArray.push_back(cornerPoints[1][0]);
	textureArray.push_back(cornerPoints[0][1]);
	textureArray.push_back(cornerPoints[1][0]);
	textureArray.push_back(cornerPoints[0][1]);
	textureArray.push_back(cornerPoints[1][1]);
}

void CubieRenderer::TranscribeToFloatArray(std::vector<glm::vec3>& vecArray, float* floatArray)
{
	// Schreibt alle Werte hintereinander weg
	int writingCounter = 0;
	for (int i = 0; i < 36; i++)
	{
		for (int coord = 0; coord < 3; ++coord)
		{
			floatArray[writingCounter++] = vecArray[i][coord];
		}
	}
}