#include "KeyboardObserver.h"
#include "GLFW/glfw3.h"

KeyboardObserver::KeyboardObserver() : KeyboardObserver(nullptr, -1)
{

}

KeyboardObserver::KeyboardObserver(GLFWwindow* window, int key)
{
	m_window = window;
	m_key = key;

	m_isDown = false;
	m_wasPressed = false;
}

void KeyboardObserver::Update()
{
	bool isDown = glfwGetKey(m_window, m_key) == GLFW_PRESS;

	// wurde nur gedr�ckt
	m_wasPressed = isDown && !m_isDown;

	// wird gedr�ckt gehalten
	m_isDown = isDown;
}

