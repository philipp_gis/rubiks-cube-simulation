#version 330

uniform mat4 transformation;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inTexture;

out vec3 sColor;
out vec2 sTexture;

void main() 
{
   gl_Position = transformation * vec4(inPosition, 1.0f);
   sColor = inColor;
   sTexture = (inTexture.xy + vec2(0.5f));
}