#pragma once

#include "GameInterface.h"
#include "CubieRenderer.h"
#include "InputSystem.h"

#include <glm/ext/quaternion_float.hpp>

using namespace std;

class Build : public GameInterface
{
public:
	// GameInterface Schnittstelle
	virtual void Initialize(GLFWwindow* window);
	virtual void Render(float aspectRatio);
	virtual void ClearResources();
	virtual void Update(double deltaTime);

private:

	// ATTRIBUTES

	CubieRenderer m_CubieRenderer;
	InputSystem m_Input;

	glm::quat m_OrientationQuaternion;
	glm::quat m_AnimationQuaternion;

	glm::mat4 m_vpMatrix;
	glm::mat4 m_mvpMatrix;

	glm::mat4 m_AnimationMatrix[3][3][3];
	glm::mat4 m_ModelMatrix[2][3][3][3];

	float m_TurningAngle;
	float m_CurrentTime;

	bool m_IsAnimation;

	int m_CurrentAxis;
	float m_CurrentDirection;
	int m_CurrentDisc;



	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
	enum state { idle, click, hit, direction, animation };
	state m_CurrentState;
	glm::vec2 m_startPoint;
	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]



	// METHODS

	// Initialize
	void InitializeControls();
	void InitializeModel();

	// Render
	void MakeMVPMatrix(float aspectRatio);
	void ModelRendering();

	// Update
	void Animate(double& deltaTime);
	void ControlHandler(double& deltaTime);

	// Helper Methods
	void ResetCubeView();
	void PanCamera(float xVelocity, float yVelocity, double deltaTime);
	void DiscRotation(int axis, float direction, int disc);
	void AnimationRotation(int axis, float direction, int disc);
	void ModelTransformation(int axis, float direction, int disc);
	void MouseControl();
};
