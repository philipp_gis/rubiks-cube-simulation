struct IUnknown; // Workaround f�r Bug mit Windows.h

#include "Build.h"
#include <Windows.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

using namespace std;



// [ INTERFACE ]

void Build::Initialize(GLFWwindow* window)
{
	// Welches Fenster soll betrachtet werden
	m_Input.SetWindow(window);

	// Welche Tasten sollen vom Observer beobachtet werden
	InitializeControls();

	// Erzeugt Cubie Geometrie und Farben
	m_CubieRenderer.Initialize();

	// Einheitsquartenion
	m_OrientationQuaternion = glm::quat(1.0f, glm::vec3(0.0f));
	m_AnimationQuaternion = glm::quat(1.0f, glm::vec3(0.0f));

	// Modelarray initialisieren
	InitializeModel();

	// Animationszustand setzen
	m_IsAnimation = false;

	m_TurningAngle = 0.0f;
	m_CurrentTime = 0.0f;

	m_CurrentAxis = NULL;
	m_CurrentDirection = NULL;
	m_CurrentDisc = NULL;



	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
	m_CurrentState = idle;
	m_startPoint = glm::vec2(0.0f);
	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
}

void Build::Render(float aspectRatio)
{
	// Erzeugt die MVP-Matrix
	MakeMVPMatrix(aspectRatio);

	// Rendert den Cube
	ModelRendering();
}

void Build::ClearResources()
{
	// F�hrt in sauberen Endzustand
	m_CubieRenderer.ClearResources();
}

void Build::Update(double deltaTime)
{
	if (m_IsAnimation)
	{
		// F�hrt eine Scheibendrehung durch
		Animate(deltaTime);
	}
	else
	{
		// �berpr�ft kontinuierlich ob und welche Tasteneingabe gemacht wurde
		m_Input.Update();

		// Was soll bei welcher Eingabe passieren
		ControlHandler(deltaTime);
	}



	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
	MouseControl();
	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
}



//  [ METHODS ]

// INITILIZE

void Build::InitializeControls()
{
	// W�rfelansicht drehen, reseten
	m_Input.ObserveKey(GLFW_KEY_SPACE);
	m_Input.ObserveKey(GLFW_KEY_RIGHT);
	m_Input.ObserveKey(GLFW_KEY_LEFT);
	m_Input.ObserveKey(GLFW_KEY_UP);
	m_Input.ObserveKey(GLFW_KEY_DOWN);

	// W�rfelscheiben drehen
	m_Input.ObserveKey(GLFW_KEY_Q);
	m_Input.ObserveKey(GLFW_KEY_W);
	m_Input.ObserveKey(GLFW_KEY_E);
	m_Input.ObserveKey(GLFW_KEY_A);
	m_Input.ObserveKey(GLFW_KEY_D);
	m_Input.ObserveKey(GLFW_KEY_Z);					// Z statt Y -> Englisches Layout
	m_Input.ObserveKey(GLFW_KEY_X);
	m_Input.ObserveKey(GLFW_KEY_C);

	// Scheiben der Y und Z Achse drehen
	m_Input.ObserveKey(GLFW_KEY_LEFT_SHIFT);
	m_Input.ObserveKey(GLFW_KEY_LEFT_CONTROL);
}

void Build::InitializeModel()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				m_ModelMatrix[0][i][j][k] = glm::toMat4(m_OrientationQuaternion);
				m_ModelMatrix[1][i][j][k] = glm::toMat4(m_OrientationQuaternion);
				m_AnimationMatrix[i][j][k] = glm::toMat4(m_AnimationQuaternion);
			}
		}
	}
}

void Build::MakeMVPMatrix(float aspectRatio)
{
	// Projektionsmatrix erzeugen
	float fieldOfView = 45.0f;
	float nearPlane = 0.1f;
	float farPlane = 100.0f;
	glm::mat4 projection = glm::perspective(glm::radians(fieldOfView), aspectRatio, nearPlane, farPlane);

	// Viewmatrix erzeugen
	glm::vec3 cameraPosition(0.0f, 0.0f, -9.0f);
	glm::vec3 cameraTarget(0.0f, 0.0f, 0.0f);
	glm::vec3 upVector(0.0f, 1.0f, 0.0f);
	glm::mat4 view = glm::lookAt(cameraPosition, cameraTarget, upVector);

	// VP Matrix erzeugen
	m_vpMatrix = projection * view;

	// Modelmatrix erzeugen
	glm::mat4 model(m_OrientationQuaternion);

	// MVP Matrix erzeugen
	m_mvpMatrix = m_vpMatrix * model;
}



// RENDER

void Build::ModelRendering()
{
	// Versatz zwischen Cubies erzeugen
	float offset = m_CubieRenderer.GetCubieExtension() + 0.01f;

	// Model Rendering
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			for (int k = 0; k < 3; ++k)
			{
				glm::mat4 animationRotation = m_AnimationMatrix[i][j][k];
				glm::mat4 cubiePosition = glm::translate(m_mvpMatrix * animationRotation, glm::vec3((i - 1) * offset, (j - 1) * offset, (k - 1) * offset));
				glm::mat4 modelRotation = m_ModelMatrix[0][i][j][k];

				glm::mat4 renderCube = cubiePosition * modelRotation;

				m_CubieRenderer.Render(renderCube);
			}
		}
	}
}



// UPDATE

void Build::Animate(double& deltaTime)
{
	// Animation
	m_TurningAngle = glm::radians(180.0f) * (float)deltaTime;
	m_CurrentTime += deltaTime;

	AnimationRotation(m_CurrentAxis, m_CurrentDirection, m_CurrentDisc);

	// Zur�cksetzen der Animation
	if (m_CurrentTime > 0.5)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				for (int k = 0; k < 3; k++)
				{
					m_AnimationMatrix[i][j][k] = glm::toMat4(m_AnimationQuaternion);
				}
			}
		}

		// Modeltransformation
		ModelTransformation(m_CurrentAxis, m_CurrentDirection, m_CurrentDisc);

		m_IsAnimation = false;
		m_CurrentTime = 0.0f;
	}
}

void Build::ControlHandler(double& deltaTime)
{
	// TASTATURSTEUERUNG

	// Kamerasteuerung
	if (m_Input.WasKeyPressed(GLFW_KEY_SPACE)) { ResetCubeView(); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); };
	if (m_Input.IsKeyDown(GLFW_KEY_UP)) { PanCamera(glm::radians(90.0f), 0.0f, deltaTime); }
	if (m_Input.IsKeyDown(GLFW_KEY_DOWN)) { PanCamera(glm::radians(-90.0f), 0.0f, deltaTime); }
	if (m_Input.IsKeyDown(GLFW_KEY_RIGHT)) { PanCamera(0.0f, glm::radians(90.0f), deltaTime); }
	if (m_Input.IsKeyDown(GLFW_KEY_LEFT)) { PanCamera(0.0f, glm::radians(-90.0f), deltaTime); }

	// Drehung der Y-Scheiben
	if (m_Input.IsKeyDown(GLFW_KEY_LEFT_SHIFT))
	{
		if (m_Input.WasKeyPressed(GLFW_KEY_Q)) { DiscRotation(1, -1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_A)) { DiscRotation(1, -1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_Z)) { DiscRotation(1, -1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_E)) { DiscRotation(1, 1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_D)) { DiscRotation(1, 1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_C)) { DiscRotation(1, 1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
	}

	// Drehung der Z-Scheiben
	else if (m_Input.IsKeyDown(GLFW_KEY_LEFT_CONTROL))
	{
		if (m_Input.WasKeyPressed(GLFW_KEY_Q)) { DiscRotation(2, -1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_A)) { DiscRotation(2, -1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_Z)) { DiscRotation(2, -1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_E)) { DiscRotation(2, 1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_D)) { DiscRotation(2, 1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_C)) { DiscRotation(2, 1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
	}

	// Drehung der X-Scheiben
	else
	{
		if (m_Input.WasKeyPressed(GLFW_KEY_Q)) { DiscRotation(0, -1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_W)) { DiscRotation(0, -1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_E)) { DiscRotation(0, -1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_Z)) { DiscRotation(0, 1.0f, -1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_X)) { DiscRotation(0, 1.0f, 0); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
		if (m_Input.WasKeyPressed(GLFW_KEY_C)) { DiscRotation(0, 1.0f, 1); PlaySound(TEXT("woosh.wav"), NULL, SND_ASYNC | SND_FILENAME); }
	}



	// MAUSSTEUERUNG

	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
	if (m_Input.IsLeftMouseButtonDown()) { m_CurrentState = click; }
	else { m_CurrentState = idle; }
	// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]

	if (m_Input.IsRightMouseButtonDown())
	{
		// Kamera drehen

		double xPos;
		double yPos;

		m_Input.GetMousePosition(xPos, yPos);

		if (-1.0 < xPos && xPos < 1.0 && -1.0 < yPos && yPos < 1.0)
		{
			PanCamera(yPos, xPos, deltaTime * 3);
		}
	}
}



// [ HELFERMETHODEN ]

// Kamerasteuerung
void Build::ResetCubeView()
{
	m_OrientationQuaternion = glm::quat(1.0f, glm::vec3(0.0f, 0.0f, 0.0f));
}

void Build::PanCamera(float xVelocity, float yVelocity, double deltaTime)
{
	glm::quat velocityQuaternion = glm::quat(0.0f, glm::vec3(xVelocity, yVelocity, 0.0f));

	m_OrientationQuaternion += 0.5f * ((float)deltaTime) * velocityQuaternion * m_OrientationQuaternion;
	m_OrientationQuaternion = normalize(m_OrientationQuaternion);
}



// Tastatursteuerung
void Build::DiscRotation(int axis, float direction, int disc)
{
	m_CurrentAxis = axis;
	m_CurrentDirection = direction;
	m_CurrentDisc = disc;

	float greatesValue = 0;

	// Achse mit gr��ten Wert bestimmen
	for (int i = 0; i < 3; i++)
	{
		if (std::abs(m_mvpMatrix[i][axis]) >= std::abs(greatesValue))
		{
			greatesValue = m_mvpMatrix[i][axis];
			m_CurrentAxis = i;
		}
	}

	// Richtung der Kamera bestimmen
	if (greatesValue < 0)
	{
		m_CurrentDirection *= -1.0f;
		m_CurrentDisc *= -1;
	}

	// In Animationszustand wechseln
	m_IsAnimation = true;
}

void Build::AnimationRotation(int axis, float direction, int disc)
{
	float angle = m_TurningAngle * direction;

	// X Scheiben verdrehen
	if (axis == 0)
	{
		glm::vec3 eulerAngles(angle, 0, 0);
		glm::mat4 rotationMatrix = glm::toMat4(glm::quat(eulerAngles));

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_AnimationMatrix[disc + 1][i][j] = rotationMatrix * m_AnimationMatrix[disc + 1][i][j];
			}
		}
	}

	// Y Scheiben verdrehen
	if (axis == 1)
	{
		glm::vec3 eulerAngles(0, angle, 0);
		glm::mat4 rotationMatrix = glm::toMat4(glm::quat(eulerAngles));

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_AnimationMatrix[i][disc + 1][j] = rotationMatrix * m_AnimationMatrix[i][disc + 1][j];
			}
		}
	}

	// Z Scheiben verdrehen
	if (axis == 2)
	{
		glm::vec3 eulerAngles(0, 0, angle);
		glm::mat4 rotationMatrix = glm::toMat4(glm::quat(eulerAngles));

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_AnimationMatrix[i][j][disc + 1] = rotationMatrix * m_AnimationMatrix[i][j][disc + 1];
			}
		}
	}
}

void Build::ModelTransformation(int axis, float direction, int disc)
{
	float angle = glm::radians(90.0f) * direction;

	// X-Achse
	if (axis == 0)
	{
		glm::vec3 EulerAngles(angle, 0, 0);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		// Rotation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_ModelMatrix[1][(disc + 1)][i][j] = RotationMatrix * m_ModelMatrix[0][(disc + 1)][i][j];
			}
		}

		// Translation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				glm::vec4 pos = glm::vec4(disc, i - 1, j - 1, 1);
				glm::vec4 result = RotationMatrix * pos;
				int iNew = (int)roundf(result.y) + 1;
				int jNew = (int)roundf(result.z) + 1;
				m_ModelMatrix[0][(disc + 1)][iNew][jNew] = m_ModelMatrix[1][(disc + 1)][i][j];
			}
		}
	}

	// Y-Achse
	if (axis == 1)
	{
		glm::vec3 EulerAngles(0, angle, 0);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		// Rotation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_ModelMatrix[1][i][(disc + 1)][j] = RotationMatrix * m_ModelMatrix[0][i][(disc + 1)][j];
			}
		}

		// Translation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				glm::vec4 pos = glm::vec4(i - 1, disc, j - 1, 1);
				glm::vec4 result = RotationMatrix * pos;
				int iNew = (int)roundf(result.x) + 1;
				int jNew = (int)roundf(result.z) + 1;
				m_ModelMatrix[0][iNew][(disc + 1)][jNew] = m_ModelMatrix[1][i][(disc + 1)][j];
			}
		}
	}

	// Z-Achse
	if (axis == 2)
	{
		glm::vec3 EulerAngles(0, 0, angle);
		glm::mat4 RotationMatrix = glm::toMat4(glm::quat(EulerAngles));

		// Rotation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				m_ModelMatrix[1][i][j][(disc + 1)] = RotationMatrix * m_ModelMatrix[0][i][j][(disc + 1)];
			}
		}

		// Translation
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				glm::vec4 pos = glm::vec4(i - 1, j - 1, disc, 1);
				glm::vec4 result = RotationMatrix * pos;
				int iNew = (int)roundf(result.x) + 1;
				int jNew = (int)roundf(result.y) + 1;
				m_ModelMatrix[0][iNew][jNew][(disc + 1)] = m_ModelMatrix[1][i][j][(disc + 1)];
			}
		}
	}
}



// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
void Build::MouseControl()
{
	if (m_CurrentState == idle)
	{
		m_startPoint = glm::vec2(0.0f);
	}

	if (m_CurrentState == click)
	{
		// WELCHE RICHTUNG WIRD GEDREHT

		// Fensterposition
		double screenX = NULL;
		double screenY = NULL;
		m_Input.GetMousePosition(screenX, screenY);

		// Startposition
		if (m_startPoint == glm::vec2(0.0f))
		{
			m_startPoint[0] = screenX;
			m_startPoint[1] = screenY;
		}

		// Endposition
		glm::vec2 screenPosition;
		screenPosition[0] = screenX;
		screenPosition[1] = screenY;

		// Distanz zwische Start und Endposition
		float distanc = glm::length(screenPosition - m_startPoint);

		// Richtung des Mausziehens
		float cursorDirection = glm::angle(glm::quat(0.0f, glm::vec3(screenX, screenY, 0.0f)));

		if (distanc > 0.1f)
		{
			cout << "drehen!" << endl;
		}



		// WO WIRD DRAUF GEKLICKT

		glm::vec3 position;
		glm::vec3 direction;

		m_Input.GetPickingRay(m_mvpMatrix, position, direction);

		// EBENENSCHNITT

		// Normalenvektoren der Ebenen
		glm::vec3 nvLeft(1.0f, m_mvpMatrix[1][3], m_mvpMatrix[2][3]);
		glm::vec3 nvRight(-1.0f, m_mvpMatrix[1][3], m_mvpMatrix[2][3]);
		glm::vec3 nvTop(m_mvpMatrix[0][3], 1.0f, m_mvpMatrix[2][3]);
		glm::vec3 nvDown(m_mvpMatrix[0][3], -1.0f, m_mvpMatrix[2][3]);
		glm::vec3 nvBack(m_mvpMatrix[0][3], m_mvpMatrix[1][3], 1.0f);
		glm::vec3 nvFront(m_mvpMatrix[0][3], m_mvpMatrix[1][3], -1.0f);

		glm::vec3 nvArray[6]{ nvBack, nvFront, nvRight, nvLeft, nvDown, nvTop };

		// Welche Ebene wird benuzt
		float plane = NULL;
		int index = 0;
		for (int i = 0; i < 6; i++)
		{
			plane = glm::dot(direction, nvArray[i]);

			if (plane < 0)
			{
				index = i;
				break;
			}
		}

		if (index == 0) { cout << " back " << endl; }
		if (index == 1) { cout << " front " << endl; }
		if (index == 2) { cout << " right " << endl; }
		if (index == 3) { cout << " left " << endl; }
		if (index == 4) { cout << " down " << endl; }
		if (index == 5) { cout << " top " << endl; }

		// Schnittpunkt berechnen
		glm::vec3 ov = position + direction;
		float alpha = glm::dot(ov - nvArray[index], nvArray[index]);
		glm::vec3 intersection = position + (alpha * direction);

		// Welcher Cubie wird angeklickt
		float u = NULL;
		float v = NULL;

		if (index == 0) { u = intersection[0]; v = intersection[1]; }
		if (index == 1) { u = intersection[0]; v = intersection[1]; }
		if (index == 2) { u = intersection[1]; v = intersection[2]; }
		if (index == 3) { u = intersection[1]; v = intersection[2]; }
		if (index == 4) { u = intersection[0]; v = intersection[2]; }
		if (index == 5) { u = intersection[0]; v = intersection[2]; }

		u = (int)(u * 100);
		v = (int)(v * 100);

		if (-u > -135 && -u < -45)
		{
			if (v > -135 && v < -45) { cout << " 1 " << endl; }
			if (v > -45 && v < 45) { cout << " 4 " << endl; }
			if (v > 45 && v < 135) { cout << " 7 " << endl; }
		}
		if (-u > -45 && -u < 45)
		{
			if (v > -135 && v < -45) { cout << " 2 " << endl; }
			if (v > -45 && v < 45) { cout << " 5 " << endl; }
			if (v > 45 && v < 135) { cout << " 8 " << endl; }
		}
		if (-u > 45 && -u < 135)
		{
			if (v > -135 && v < -45) { cout << " 3 " << endl; }
			if (v > -45 && v < 45) { cout << " 6 " << endl; }
			if (v > 45 && v < 135) { cout << " 9 " << endl; }
		}
		cout << " " << endl;
	}
}
// [ MAUSSTEUERUNG NICHT FERTIG IMPLEMENTIERT ]
