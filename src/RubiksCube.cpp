#include "GameInterface.h"
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Build.h"

Build gBuild;

// Aktuell ausgew�hltes Interface
GameInterface* gUsedInterface;

// Inizialisiert GLFW und GLEW und Erzeugung von einem Fenster
GLFWwindow* InitializeSystem()
{
	// GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Fenster
	GLFWwindow* window = glfwCreateWindow(1024, 768, "Rubiks Cube", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	// GLEW
	glewExperimental = GL_TRUE;
	glewInit();

	gUsedInterface->Initialize(window);

	return window;
}

// Erzeugen einer Updatefunktion der Logik und Grafik
void RunCoreloop(GLFWwindow* window)
{
	double lastTime = glfwGetTime();
	double timeDifference = 0.0f;

	while (!glfwWindowShouldClose(window))
	{
		// Aktiviert Maus und Tastatureingaben
		glfwPollEvents();														

		gUsedInterface->Update(timeDifference);

		// Breite und H�he des erzeugten Fenster in Pixel
		int screenWidth, screenHeight;
		glfwGetFramebufferSize(window, &screenWidth, &screenHeight);			
		
		// Passt Fenstergr��e dynamisch an
		float aspectRatio = (float)screenWidth / (float)screenHeight;
		glViewport(0, 0, screenWidth, screenHeight);							

		// Aktiviert den Tiefentest
		glEnable(GL_DEPTH_TEST);												
		glDepthFunc(GL_LEQUAL);

		// Hintergrundfarbe
		glClearColor(0.3f, 0.3f, 0.3f, 1.0f);									
		
		// Leert den Tiefen und Farb Buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);						

		gUsedInterface->Render(aspectRatio);

		// Wechsel zwischen Front und Endbuffer
		glfwSwapBuffers(window);												

		// Aktualisierung der Zeit
		double currentTime = glfwGetTime();										
		timeDifference = currentTime - lastTime;
		lastTime = currentTime;
	}
}

// Beendet das Programm endg�ltig
void ShutdownSystem()
{
	// Beendet Programm sauber
	gUsedInterface->ClearResources();
	glfwTerminate();															
}

// Initialisierung des Programms
int main()
{
	gUsedInterface = &gBuild;
	GLFWwindow* window = InitializeSystem();
	RunCoreloop(window);													
	ShutdownSystem();
}