#pragma once

struct GLFWwindow;
class KeyboardObserver
{
public:
	// Beobachtet Taste in einem Fenster
	KeyboardObserver();
	KeyboardObserver(GLFWwindow* window, int key);
	
	// �berpr�ft ob eine Taste gedr�ckt wurde oder gedr�ckt gehalten wird
	void Update();

	bool m_isDown;
	bool m_wasPressed;

private:
	GLFWwindow* m_window;
	int m_key;
};