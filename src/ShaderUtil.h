#pragma once
#include <GL/glew.h>
#include <string>

class ShaderUtil
{
public:
	// Setzt VertexShader und FragmentShader zusammen
	static GLuint CreateShaderProgram(const char* vertexFilename, const char* fragmentFilename);

private:
	// L�dt eine Datei
	static std::string LoadFile(const char* fileName);
	
	// Fehlermeldungen bei kompilierung eines Shaders
	static void PrintShaderLog(GLuint shader);

	// Fehlermelungen bei der Linken der Shader
	static void PrintProgramLog(GLuint program);

	// Fehlermeldungen von OpenGL
	static void PrintOGlErrors();	
};