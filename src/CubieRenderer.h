#pragma once
#include <glm/mat4x4.hpp>
#include <GL/glew.h>
#include <vector>

class CubieRenderer
{
public:
	// Anfangszustand
	void Initialize();

	// Rendert eine übergebene Matrix
	void Render(const glm::mat4& transfomationMatrix);

	// gesäuberter Endzustand
	void ClearResources();

	float GetCubieExtension() const { return 2.0f * m_offset; }

private:
	const float m_offset = 0.5f;
	float m_baseColor = 1.0f;

	// Drei VertexBuffer: Position der Seiten, Werte der Farben, Position der Texturen
	void AddSidePosition(int sideType, int direction, std::vector<glm::vec3>& positionArray);
	void AddSideColor(int sideType, int direction, std::vector<glm::vec3>& colorArray);
	void AddSideTexture(int sideType, int direction, std::vector<glm::vec3>& textureArray);
	
	// Wandelt ein Vec3 in ein floatArray
	void TranscribeToFloatArray(std::vector<glm::vec3>& vecArray, float* floatArray);

	GLuint m_arrayBufferObject;
	GLuint m_vertexBufferObject[3];
	GLuint m_shaderProgram;
	GLuint m_texture;
	GLint m_transformLocation;
};
